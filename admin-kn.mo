��          �       �      �  >   �  &   �  �   �  +   �  <   �      �          2  �   Q     �  =   �     1     E  6   U     �  T   �        #     �   A  �     J   �  )     �   ;  �   3  1   	  Q  :	     �
  {        �  T   �  W   �  �  I  *   �  �     Z   �  Q   �  �   Q  P   !  �   r  Z   .  V   �  P  �  O  1    �  *   �   %s comment permanently deleted %s comments permanently deleted <strong>%1$s</strong> by <em>%2$s</em> <strong>The parent theme could not be found.</strong> You will need to install the parent theme, <strong>%s</strong>, before you can use this child theme. Add menu items from the column on the left. An error occurred while updating %1$s: <strong>%2$s</strong> Background AttachmentAttachment Error in moving to trash. Error in restoring from trash. Hi, this is a comment.
To delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them. Loading&hellip; Post submitted. <a target="_blank" href="%s">Preview post</a> Repeat New Password Repeat Password Saving is disabled: %s is currently editing this post. The package contains no files. The plugin <code>%s</code> has been <strong>deactivated</strong> due to an error: %s The theme contains no files. This item has already been deleted. When you assign multiple categories or tags to a post, only one can show up in the permalink: the lowest numbered category. This applies if your custom structure includes <code>%category%</code> or <code>%tag%</code>. WordPress is not notifying any <a href="http://codex.wordpress.org/Update_Services">Update Services</a> because of your site&#8217;s <a href="%s">visibility settings</a>. You are about to delete this link '%s'
  'Cancel' to stop, 'OK' to delete. http://wordpress.org/plugins/hello-dolly/ PO-Revision-Date: 2013-07-27 07:03:46+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Administration
 %s ಟಿಪ್ಪಣಿ ಯನ್ನು ಶಾಶ್ವತವಾಗಿ ಅಳಿಸಲಾಗಿದೆ.
 %s ಟಿಪ್ಪಣಿ ಗಳನ್ನು ಶಾಶ್ವತವಾಗಿ ಅಳಿಸಲಾಗಿದೆ.
 <em>%2$s</em> ರಿಂದ <strong>%1$s</strong>  <strong> ಪೇರೆಂಟ್ ಥೀಮ್ ಅನ್ನು ಹುಡುಕಲಾಗಲಿಲ್ಲ.</strong> ಈ ಚೈಲ್ಡ್ ಥೀಮ್ ಅನ್ನು ನೀವು ಬಳಸುವ ಮುನ್ನ, ಪೇರೆಂಟ್ ಥೀಮ್ <strong>%s</strong> ಅನ್ನು ಸ್ಥಾಪಿಸುವ ಅಗತ್ಯವಿದೆ.   ಬಲ ಬದಿಯ ಅಂಕಣದಿಂದ ಆಯ್ಕೆಪಟ್ಟಿಯ ಅಂಶಗಳನ್ನು ಸೇರಿಸಿ.  %1$s ಅನ್ನು ಉನ್ನತೀಕರಿಸುವಾಗ ದೋಷ ಉಂಟಾಗಿದೆ : <strong>%2$s</strong>  ಲಗತ್ತು  ಅನುಪಯುಕ್ತಕ್ಕೆ ಸಾಗಿಸುವಲ್ಲಿ ದೋಷ  ಅನುಪಯುಕ್ತದಿಂದ ಮರುಬಳಸುವಲ್ಲಿ ದೋಷ  ನಮಸ್ತೆ, ಇದು ಒಂದು ಟಿಪ್ಪಣಿ.
ಟಿಪ್ಪಣಿಯನ್ನು ಅಳಿಸಲು, ಲಾಗಿನ್ ಆಗಿ ಮತ್ತು ಲೇಖನದ ಟಿಪ್ಪಣಿಯನ್ನು  ನೋಡಿ. ಅಲ್ಲೇ ನೀವು ಅವುಗಳನ್ನು ಸಂಪಾದಿಸುವ ಅಥವಾ ಅಳಿಸುವ ಆಯ್ಕೆಗಳನ್ನು ಕಾಣುತ್ತೀರಿ.
  ತುಂಬುತ್ತಿದೆ&hellip;  ಲೇಖನವನ್ನು ಸಲ್ಲಿಸಲಾಗಿದೆ. <a target="_blank" href="%s">ಲೇಖನದ ಮುನ್ನೋಟ</a>   ಹೊಸ ಪ್ರವೇಶಪದವನ್ನು ಪುನಾರಾವರ್ತಿಸಿ  ಪ್ರವೇಶಪದವನ್ನು ಪುನಾರಾವರ್ತಿಸಿ   ಉಳಿಸುವಿಕೆ ನಿಷ್ಕ್ರಿಯವಾಗಿದೆ: %s ಅವರು ಪ್ರಸ್ತುತ ಈ ಲೇಖನವನ್ನು ಸಂಪಾದಿಸುತ್ತಿದ್ದಾರೆ.


  ಈ ಕಂತೆಯಲ್ಲಿ ಯಾವುದೇ ಕಡತಗಳಿಲ್ಲ.  %s ದೋಷದ ಕಾರಣದಿಂದಾಗಿ <code>%s</code> ಪ್ಲಗ್ ಇನ್ ಅನ್ನು <strong>ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲಾಗಿದೆ.</strong>  ಈ ಥೀಮ್ ಯಾವುದೇ ಕಡತಗಳನ್ನು ಹೊಂದಿಲ್ಲ.  ಈ ವಸ್ತುವು ಈಗಾಗಲೇ ಅಳಿಸಲ್ಪಟ್ಟಿದೆ.  ನೀವು ಹಲವು ವಿಭಾಗಗಳನ್ನು ಅಥವಾ ಟ್ಯಾಗ್ ಗಳನ್ನು ಲೇಖನವೊಂದಕ್ಕೆ ಹೊಂದಿಸಿದಾಗ, ಕೇವಲ ಒಂದು ಮಾತ್ರ permalink ನಲ್ಲಿ ಪ್ರದರ್ಶಿಸಲ್ಪಡುತ್ತದೆ: ಅತಿ ಕಡಿಮೆ ಸಂಖ್ಯೆಯುಳ್ಳ ವಿಭಾಗ. ಇದು ನಿಮ್ಮ ಸ್ವಯೋಜಿತ ಸ್ವರೂಪ <code>%category%</code> ಅಥವಾ <code>%tag%</code> ಅನ್ನು ಒಳಗೊಂಡಾಗ ಅನ್ವಯಿಸುತ್ತದೆ.  ವರ್ಡ್ ಪ್ರೆಸ್ ನಿಮ್ಮ ಜಾಲತಾಣದ  <a href="%s">ಗೋಚರತೆ ಸಂಯೋಜನೆಗಳು</a> ಇಂದಾಗಿ, ಯಾವುದೇ <a href="http://codex.wordpress.org/Update_Services">ನವೀಕರಣ ಸೌಕರ್ಯ</a> ಗಳನ್ನು ಸೂಚಿಸುತ್ತಿಲ್ಲ.  ನೀವು '%s' ಲಿಂಕ್ ಅನ್ನು ಅಳಿಸಲಿದ್ದೀರಿ 
ಅಳಿಸಲು 'ಸರಿ' ಅನ್ನು ಒತ್ತಿ, ಸ್ಥಗಿತಗೊಳಿಸಲು 'ರದ್ದುಗೊಳಿಸಿ' ಅನ್ನು ಒತ್ತಿ.
  http://wordpress.org/plugins/hello-dolly/  